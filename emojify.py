#!/usr/bin/env python3
# Emojify - String to emoji-text converter. https://github.com/imsofi/emojify
# Copyright (C) 2018-2020 @imSofi - licensed under the MIT License

import click
import pyperclip # type: ignore

VERSION = '3.3.1'
MAX_GROUP_LENGTH = 2000
NUMBERS = ('zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine')
SIGNS = {'#': 'hash', '!': 'exclamation', '?': 'question', '*': 'asterisk'}

class emojify:
    """String to emoji converter."""
    @staticmethod
    def to_emoji(text: str) -> list:
        """Returns a processed list containing the emoji equivilant of
        each character in a given input string.

        If a character cant be turned into an emoji it gets skipped
        silently."""
        output = []
        for letter in text.lower():
            if ord(letter) in range(97, 123): # [a-z]
                output.append(f":regional_indicator_{letter}:")
            elif letter.isspace():
                output.append('   ')
            elif letter.isdigit():
                output.append(f':{NUMBERS[int(letter)]}:')
            elif letter in SIGNS:
                output.append(f':{SIGNS[letter]}:')
        return output

    @staticmethod
    def group_list_by_length(lst: list, max_length: int) -> list:
        """ Groups list up by the items in a list,
        measuring each group to fit into a total max length."""
        output = []
        start_num = 0
        for end_num in range(len(lst)):
            if len(' '.join(lst[start_num:end_num])) > max_length:
                output.append(' '.join(lst[start_num:end_num-1]))
                start_num = end_num - 1
        output.append(' '.join(lst[start_num:len(lst)]))
        return output


@click.command(context_settings=dict(help_option_names=['-h', '--help']))
@click.argument('text', nargs=-1)
@click.option('-c', '--copy', is_flag=True, help="Send output to clipboard.")
@click.option('-p', '--paste', is_flag=True, help="Use clipboard as input.")
@click.version_option(VERSION, '-v', '--version')
def main(text: tuple, copy: bool, paste: bool):
    """Emojify: Simple to use string to emoji text converter.

    TEXT is the input text to be processed. Unless another
    input option is used, which will overwrite this argument.
    """
    if paste:
        input_string = pyperclip.paste()
    elif not text:
        input_string = input('> ')
    else:
        input_string = ' '.join(text)

    output_list = emojify.to_emoji(input_string)
    output_string = ' '.join(output_list)

    if copy:
        pyperclip.copy(output_string)
    elif len(output_string) > MAX_GROUP_LENGTH:
        click.echo('\n{}\n'.format(
            '\n\n'.join(emojify.group_list_by_length(output_list, MAX_GROUP_LENGTH))
        ))
    else:
        click.echo(output_string)

if __name__ == '__main__':
    main()
